#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "mpi_navier.h"

int my_rank;
int nprocs;
int nprocs_y;
int nprocs_x;
int my_rank_x;
int my_rank_y;
int prev_y;
int next_y;
int next_x;
int prev_x;
int imax_full;
int jmax_full;
int gbl_i_begin;
int gbl_j_begin;

std::vector<double*> dat_ptrs{};
std::vector<bool> dat_dirty{};

void mpi_setup(int *imax, int *jmax) {
	// Initialise: get #of processes and process id
    MPI_Init(nullptr, nullptr);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    // Create MPI cartesian communicator
	// Check for compatible number of processes
	// Figure out neighbours
	// Figure out process X,Y coordinates
    int dims[2] {0, 0};
    MPI_Dims_create(nprocs, 2, dims);
    nprocs_x = dims[0];
    nprocs_y = dims[1];

    int periods[2] = {0, 0}; // non periodic
    int reorder = 0;
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &MPI_COMM_CART);

    MPI_Cart_shift(MPI_COMM_CART, 0, 1, &prev_x, &next_x);
    MPI_Cart_shift(MPI_COMM_CART, 1, 1, &prev_y, &next_y);
    
    int coords[2];
    MPI_Cart_coords(MPI_COMM_CART, my_rank, 2, coords);

    my_rank_x = coords[0];
    my_rank_y = coords[1];
	
	// Save original full sizes in x and y directions
    imax_full = *imax;
    jmax_full = *jmax;

	// Modify imax and jmax (pay attention to integer divisions's rounding issues!)
	*imax = imax_full/nprocs_x;
	*jmax = jmax_full/nprocs_y;

	// Figure out beginning i and j index in terms of global indexing
    gbl_i_begin = my_rank_x * imax_full/nprocs_x;
    gbl_j_begin = my_rank_y * jmax_full/nprocs_y;
	
    if (my_rank_x == nprocs_x-1) *imax += imax_full%nprocs_x;
    if (my_rank_y == nprocs_y-1) *jmax += jmax_full%nprocs_y;

	// Let's set up MPI Datatypes
    MPI_Type_vector(*imax + 2 , 1, 1, MPI_DOUBLE, &row);
    MPI_Type_vector(*jmax + 2 , 1, *imax + 2, MPI_DOUBLE, &column);
    MPI_Type_commit(&row);
    MPI_Type_commit(&column);
}

void add_dat_ptr(double* dat_ptr) {
    dat_ptrs.push_back(dat_ptr);
    dat_dirty.push_back(true);
}

void exchange_halo(int imax, int jmax, double *arr) {
	int dirty = -1;
	for (int i = 0; i < dat_ptrs.size(); i++) {
		if ((double*)arr == dat_ptrs[i]) {
			if (dat_dirty[i]) 
                dirty = i;
			break;
		}
	}
	if (dirty!=-1) {
		// Exchange halos: prev_y, next_y, prev_x, next_x
        MPI_Sendrecv(&arr[1],      1, column, prev_x, 0,
                     &arr[imax+1], 1, column, next_x, 0,
                     MPI_COMM_CART, MPI_STATUS_IGNORE);
        MPI_Sendrecv(&arr[imax],      1, column, next_x, 0,
                     &arr[0], 1, column, prev_x, 0,
                     MPI_COMM_CART, MPI_STATUS_IGNORE);

        MPI_Sendrecv(&arr[0+ 1*(imax+2)],     1, row, prev_y, 0,
                     &arr[(jmax+1)*(imax+2)], 1, row, next_y, 0,
                     MPI_COMM_CART, MPI_STATUS_IGNORE);

        MPI_Sendrecv(&arr[jmax*(imax+2)], 1, row, next_y, 0,
                     &arr[0],             1, column, prev_y, 0,
                     MPI_COMM_CART, MPI_STATUS_IGNORE);
        dat_dirty[dirty] = false;
	}
}

void set_dirty(int jmax, double *arr) {
	for (int i = 0; i < dat_ptrs.size(); i++) {
		if ((double*)arr == dat_ptrs[i]) {
			dat_dirty[i] = true;
			break;
		}
	}
}
