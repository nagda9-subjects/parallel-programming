#include <vector>
#include <mpi.h>

extern int my_rank;
extern int prev_y;
extern int next_y;
extern int next_x;
extern int prev_x;

extern int imax_full;
extern int jmax_full;

extern int gbl_i_begin;
extern int gbl_j_begin;

extern std::vector<double*> dat_ptrs;
extern std::vector<bool> dat_dirty;

inline MPI_Comm MPI_COMM_CART;
inline MPI_Datatype row, column;

void mpi_setup(int *imax, int *jmax);
void add_dat_ptr(double* dat_ptr);
void exchange_halo(int imax, int jmax, double *arr);
void set_dirty(int jmax, double *arr);