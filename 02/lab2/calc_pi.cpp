#include <algorithm>
#include <chrono>
#include <iostream>
#include <omp.h>
#include <random>
#include <vector>

int main() {
    std::chrono::time_point<std::chrono::high_resolution_clock> start{}, end{};
    constexpr int SHOT_NUM = 100000000;
    constexpr double BORDER = 0.5;
    int count = 0;

    start = std::chrono::high_resolution_clock::now();
#pragma omp parallel
    {
        // The random device and generator are not thread safe, so
        // set up as many random number engines as many threads are
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<double> dis(-BORDER, BORDER);
#pragma omp for reduction(+ : count)
        for (int i = 0; i < SHOT_NUM; i++) {
            double x = dis(gen);
            double y = dis(gen);
            if (x * x + y * y < BORDER * BORDER) {
                count++;
            }
        }
    }
    end = std::chrono::high_resolution_clock::now();

    double pi = 4.0 * count / static_cast<double>(SHOT_NUM);
    std::cout << "Pi is " << pi << "\n";

    auto elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << "Runtime: " << elapsed_seconds << "ms\n";
}
