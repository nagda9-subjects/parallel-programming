#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>

int main() {
    std::srand(std::time(0));
    constexpr int N = 10000;

    std::vector<float> matrix(N * N);
    std::vector<float> vector(N);
    std::vector<float> y(N);

    // fill the mat and vec
    for (int i = 0; i < N; i++) {
        vector[i] = (float)std::rand() / (float)RAND_MAX;
        for (int j = 0; j < N; j++) {
            matrix[i * N + j] = (float)std::rand() / (float)RAND_MAX;
        }
    }

    // multiply
    auto t1 = std::chrono::high_resolution_clock::now();
#pragma omp parallel for
    for (int i = 0; i < N; i++) {  // this must be the outer loop, 10X faster
        for (int j = 0; j < N; j++) {
            y[i] += matrix[i * N + j] * vector[j];
        }
    }
    auto t2 = std::chrono::high_resolution_clock::now();

    std::cout << "took " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";
    return 0;
}
