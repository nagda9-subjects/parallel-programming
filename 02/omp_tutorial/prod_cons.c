/*
**  PROGRAM: A simple SIMD producer-consumer program
**
**  PURPOSE: this is just a stupid little program to play around
**  with different ways data is shared between threads.
**
**  HISTORY: Written by Tim Mattson, April 2007.
*/

/// WARNING!!!
// In this code omp is used for CONCURRENT PROGRAMMING,
// and not PARALLEL PROGRAMMING! Although omp is capable of doing this, 
// probably should NEVER use this!

#include "omp.h"
#ifndef APPLE
    #include <malloc.h>
#endif
#include <stdio.h>
#include <stdlib.h>

#define N 10000
#define Nthreads 2

/* Some random number constants from numerical recipies */
#define SEED 2531
#define RAND_MULT 1366
#define RAND_ADD 150889
#define RAND_MOD 714025
int randy = SEED;

/* function to fill an array with random numbers */
void fill_rand(int length, double* a) {
    for (int i = 0; i < length; i++) {
        randy = (RAND_MULT * randy + RAND_ADD) % RAND_MOD;
        *(a + i) = ((double)randy) / ((double)RAND_MOD);
    }
}

/* function to sum the elements of an array */
double sum_array(int length, double* a) {
    double sum = 0.0;
    for (int i = 0; i < length; i++)
        sum += *(a + i);
    return sum;
}

int main() {
    double *A, sum, runtime;
    int numthreads, flag = 0;

    omp_set_num_threads(Nthreads);

    A = (double*)malloc(N * sizeof(double));

#pragma omp parallel
    {
#pragma omp master
        {
            numthreads = omp_get_num_threads();
            if (numthreads != 2) {
                printf("error: incorect number of threads, %d \n", numthreads);
                exit(-1);
            }
            runtime = omp_get_wtime();
        }
// Put a barrier after #pragma omp master,
// because there is no implied barrier after it by default.
#pragma omp barrier

#pragma omp sections
        {
#pragma omp section  // section for producer
            {
                fill_rand(N, A);  // produce array A
#pragma omp flush                 // after filling the A array we want to flush it so everyone can see it

#pragma omp atomic write   // protect flag variable from race conditions
                flag = 1;  // set flag: ready
#pragma omp flush(flag)
                // flush the flag so everyone can see it                                                    \
                // Flush only the flag when possible, because a general #pragma omp flush is very expensive!
            }

#pragma omp section  // section for consumer
            {
                int flag_tmp;
                do {
#pragma omp flush(flag)  // wait in a spin lock for flag to become 1 (continuously update flag)
#pragma omp atomic read  // protect flag variable from race conditions
                    flag_tmp = flag;
                } while (flag_tmp != 1);

#pragma omp flush                       // when the flag is ready update all the variables
                sum = sum_array(N, A);  // consume array A
            }
        }

#pragma omp master
        runtime = omp_get_wtime() - runtime;
    }

    printf(" with %d threads and %lf seconds, The sum is %lf \n", numthreads, runtime, sum);
}
