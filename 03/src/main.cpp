#include <iostream>
#include <iomanip>
#include "../header/sim.h"

int main() {
	Sim s;
	s.Run();

    float linSolveTimeSum = 0.0;
    float linSolveGBSum = 0.0;
    #pragma omp parallel for reduction(+ : linSolveTimeSum, linSolveGBSum)
    for (size_t i = 0; i < linSolveTimes.size(); i++) {
        linSolveTimeSum += linSolveTimes[i];
        linSolveGBSum += linSolveGB[i];
    }

    float projectTimeSum = 0.0;
    float projectGBSum = 0.0;
    #pragma omp parallel for reduction(+ : projectTimeSum, projectGBSum)
    for (size_t i = 0; i < projectTimes.size(); i++) {
        projectTimeSum += projectTimes[i];
        projectGBSum += projectGB[i];
    }

    float advectTimeSum = 0.0;
    float advectGBSum = 0.0;
    #pragma omp parallel for reduction(+ : advectTimeSum, advectGBSum)
    for (size_t i = 0; i < advectTimes.size(); i++) {
        advectTimeSum += advectTimes[i];
        advectGBSum += advectGB[i];
    }

    // std::cout << "LinSolve: " << LinSolveTimes / LinSolveCount << "s\t"
    //           << static_cast<float>((LinSolveBytes * 1e-9) / LinSolveTime) / LinSolveCount << " GB/s\n";
    std::cout << std::fixed << std::setprecision(4);
    std::cout << "Name\t\tCount\tTime(s)\tGB/s\n";
    std::cout << "LinSolve\t" << linSolveTimes.size() << "\t" << linSolveTimeSum << "\t" << linSolveGBSum / linSolveTimeSum << "\n";
    std::cout << "Project\t\t" << projectTimes.size() << "\t" << projectTimeSum << "\t" << projectGBSum / projectTimeSum << "\n";
    std::cout << "Advect\t\t" << advectTimes.size() << "\t" << advectTimeSum << "\t" << advectGBSum / advectTimeSum << "\n";
	return 0;
}
