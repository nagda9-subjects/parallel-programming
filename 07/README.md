[![Review Assignment Due Date](https://classroom.github.com/assets/deadline-readme-button-24ddc0f5d75046c5622901739e7c5dd533143b0c8e959d652212380cedb1ea36.svg)](https://classroom.github.com/a/JDga5q6T)
# assignment-7

nvc++ -fast -mp laplace2d.cpp -o laplace

nvc++ laplace2d.cpp -acc -fast -Minfo=accel -o laplace2d_acc
