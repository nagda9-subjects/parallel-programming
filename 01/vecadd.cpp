#include <chrono>
#include <iostream>
#include <omp.h>
#include <vector>

#define N 100000000

int main(int argc, const char** argv) {
    std::vector<float> a(N), b(N), c(N);
    /* Some initialisation */
    for (int i = 0; i < N; i++)
        a[i] = b[i] = i * 1.0;

    auto t1 = std::chrono::high_resolution_clock::now();
#pragma omp parallel for
    for (int i = 0; i < N; i++)
        c[i] = a[i] + b[i];

    auto t2 = std::chrono::high_resolution_clock::now();
    std::cout << "took " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds\n";

    return 0;
}
