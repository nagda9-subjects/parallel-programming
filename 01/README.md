# Assignment:

To add results, edit this file.
Time your code using chrono

Record time without extra compilation flags

Record time with -Ofast

Record time with -Ofast -fopenmp

Record time with different numbers of threads used

Upload plot of timings with 4 threads, and array sizes 1K to 1G (just keep doubling the size)
