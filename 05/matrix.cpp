#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

constexpr int N = 1000;

int main(int argc, char* argv[]) {
    srand(static_cast<uint>(time(0)));

    MPI_Init(&argc, &argv);
    MPI_Status status;
    MPI_Comm cannon_comm;
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == 0)
        std::cout << "Matrix size: " << N << "\n";

    int dims[2];
    MPI_Dims_create(size, 2, dims);
    if (dims[0] != dims[1]) {
        if (rank == 0)
            std::cerr << "The number of processors must be a square.\n";
        MPI_Finalize();
        return 0;
    }

    int rows = N / dims[0];
    double* A = new double[rows * rows];
    double* B = new double[rows * rows];
    double* C = new double[rows * rows];
    double* buf = new double[rows * rows];
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < rows; j++) {
            A[i * rows + j] = rand() / (double)RAND_MAX;
            B[i * rows + j] = rand() / (double)RAND_MAX;
            C[i * rows + j] = 0.0;
        }
    }
    int periods[2];
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 1, &cannon_comm);

    int left, right, up, down;
    MPI_Cart_shift(cannon_comm, 0, 1, &left, &right);
    MPI_Cart_shift(cannon_comm, 1, 1, &up, &down);

    double start = MPI_Wtime();
    for (int shift = 0; shift < dims[0]; shift++) {
        // Matrix multiplication
        for (int i = 0; i < rows; i++)
            for (int k = 0; k < rows; k++)
                for (int j = 0; j < rows; j++)
                    C[i * rows + j] += A[i * rows + k] * B[k * rows + j];
        if (shift == dims[0] - 1)
            break;

        // Communication
        MPI_Sendrecv(A, rows * rows, MPI_DOUBLE, left, 1, buf, rows * rows, MPI_DOUBLE, right, 1, cannon_comm, &status);
        double * tmp = buf;
        buf = A;
        A = tmp;
        MPI_Sendrecv(B, rows * rows, MPI_DOUBLE, up, 2, buf, rows * rows, MPI_DOUBLE, down, 2, cannon_comm, &status);
        tmp = buf;
        buf = B;
        B = tmp;
    }
    MPI_Barrier(cannon_comm);

    double end = MPI_Wtime();
    if (rank == 0)
        std::cout << "Time: " << end - start << "\n";

    delete[] A;
    delete[] B;
    delete[] C;
    delete[] buf;
    
    MPI_Finalize();
    return 0;
}