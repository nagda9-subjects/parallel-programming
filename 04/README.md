# assignment-4

```sh
sudo apt install libopenmpi-dev
```

## Run programs

```sh
mpirun -np 4 build/bin/mpi_hello
mpirun -np 4 build/bin/send_recv
```
