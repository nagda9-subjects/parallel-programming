#include <iostream>
#include <mpi.h>

int main(int argc, char* argv[]) {
    int numtasks, rank, len, rc;
    char hostname[MPI_MAX_PROCESSOR_NAME];

    rc = MPI_Init(&argc, &argv);
    if (rc != MPI_SUCCESS) {
        std::cout << "Error starting MPI program. Terminating.\n";
        MPI_Abort(MPI_COMM_WORLD, rc);
    }

    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int partner;
    if (rank < numtasks / 2) {
        partner = numtasks / 2 + rank;
    } else if (rank >= numtasks / 2) {
        partner = rank - numtasks / 2;
    }

    srand(time(0) + rank);
    int rand_num = rand() % 100;
    MPI_Send(&rand_num, 1, MPI_INT, partner, 0, MPI_COMM_WORLD);
    
    int recv_num;
    MPI_Recv(&recv_num, 1, MPI_INT, partner, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    std::cout << "rank=" << rank << " partner=" << partner << " sent=" << rand_num << " received=" << recv_num << "\n";

    MPI_Finalize();
}
