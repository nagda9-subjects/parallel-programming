#include <cstdlib>
#include <ctime>
#include <iostream>
#include <mpi.h>

int main(int argc, char** argv) {
    int N = 100000;

    MPI_Init(&argc, &argv);
    int numtasks, rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

    std::srand(std::time(0));

    int sum = 0;

    sum = 0;
    for (int i = 0; i < N / numtasks; i++) {
        double x = (double)(std::rand()) / (double)RAND_MAX - 0.5;
        double y = (double)(std::rand()) / (double)RAND_MAX - 0.5;
        if ((x * x + y * y) < 0.25)
            sum++;
    }

    if (rank != 0)
        MPI_Send(&sum, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    else {
        for (int i = 1; i < numtasks; i++) {
            int recv;
            MPI_Recv(&recv, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            sum += recv;
        }
        std::cout << (double)sum / (double)(N)*4.0 << std::endl;
    }

    MPI_Finalize();
    return 0;
}
